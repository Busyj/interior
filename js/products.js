(function(w,d) {
	var flag = false;


	w.onscroll = function() {scrolling()}	

	function scrolling () {
		
		var header = d.getElementsByClassName("header")[0];		
		var scrollTop =  window.pageYOffset;
		var winHieght = d.documentElement.clientHeight;
		if (scrollTop > winHieght/2) {
			header.style.paddingTop = '0';
			
		}

		if (scrollTop > winHieght)  {
			if (!flag) {				
				header.classList.add("header_fix");
				flag = true;
			}
			
		}
		else {			
			header.classList.remove("header_fix");
			header.style.paddingTop = '65px';
			flag = false;			
		}	

	}

})(window,document);
